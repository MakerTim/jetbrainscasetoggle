package nl.makertim.jetbrains.casetoggler.casing;

public class CamelCase implements Casing {

	@Override
	public String[] separate(String input) {
		boolean is$Var = false;
		if (input.charAt(0) == '$') {
			input = input.substring(1);
			is$Var = true;
		}
		String[] separated = input.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])");
		if (is$Var && separated.length > 0) {
			separated[0] = '$' + separated[0];
		}
		return separated;
	}

	@Override
	public boolean matches(String input) {
		int offset = input.charAt(0) == '$' ? 1 : 0;
		return input.substring(offset, offset + 1).toLowerCase().equals(input.substring(offset, offset + 1))
				&& !input.toLowerCase().equals(input)
				&& !input.toUpperCase().equals(input)
				&& !input.contains("_")
				&& !input.contains("-");
	}

	@Override
	public String transform(String[] input) {
		StringBuilder transformed = new StringBuilder();
		if (input != null && input.length > 0) {
			transformed = new StringBuilder(input[0].toLowerCase());
			for (int i = 1; i < input.length; i++) {
				transformed
						.append(input[i].substring(0, 1).toUpperCase())
						.append(input[i].substring(1).toLowerCase());
			}
		}
		return transformed.toString();
	}
}
