package nl.makertim.jetbrains.casetoggler.casing;

import com.intellij.lang.Language;

import java.util.HashMap;
import java.util.Map;

public interface Casing {

	Casing[] CASINGS_ORDER = new Casing[] {
			new CamelCase(),
			new PascalCase(),
			new SnakeCase(),
//			new KebabCase(),
			new UpperSnakeCase(),
//			new UpperKebabCase(),
	};

	Map<String, Casing[]> CASINGS_ORDER_BY_LANG = new HashMap<>() {{
		put("JAVA", new Casing[] {
				new CamelCase(),
				new PascalCase(),
				new SnakeCase(),
				new UpperSnakeCase(),
		});
		put("PHP", new Casing[] {
				new CamelCase(),
				new PascalCase(),
				new SnakeCase(),
				new UpperSnakeCase(),
		});
		put("PYTHON", new Casing[] {
				new CamelCase(),
				new PascalCase(),
				new SnakeCase(),
				new UpperSnakeCase(),
		});
		put("HTML", new Casing[] {
				new CamelCase(),
				new PascalCase(),
				new KebabCase(),
				new UpperKebabCase(),
		});
		put("CSS", new Casing[] {
				new CamelCase(),
				new PascalCase(),
				new KebabCase(),
				new UpperKebabCase(),
		});
	}};

	String[] separate(String input);

	boolean matches(String input);

	String transform(String[] input);

	static String transformToNextCasing(String input, Language language) {
		input = input.trim();

		Casing[] availableCasings = (CASINGS_ORDER_BY_LANG.getOrDefault(language.getID(), CASINGS_ORDER));

		int currentCasingIndex = -1;
		for (int i = 0; i < availableCasings.length; i++) {
			if (availableCasings[i].matches(input)) {
				currentCasingIndex = i;
				break;
			}
		}

		if (currentCasingIndex != -1) {
			String[] split = availableCasings[currentCasingIndex].separate(input);
			return availableCasings[(currentCasingIndex + 1) % availableCasings.length].transform(split);
		}

		if (input.toLowerCase().equals(input)) {
			return input.toUpperCase();
		}
		if (input.toUpperCase().equals(input)) {
			return input.toLowerCase();
		}
		assert input == null; // Zou niet moeten voorkomen ; nieuwe soort casing OF casing moet iets anders gebeuren?
		return input;
	}
}
