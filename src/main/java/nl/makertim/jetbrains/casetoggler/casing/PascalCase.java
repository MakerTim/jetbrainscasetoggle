package nl.makertim.jetbrains.casetoggler.casing;

public class PascalCase extends CamelCase {

	@Override
	public boolean matches(String input) {
		int offset = input.charAt(0) == '$' ? 1 : 0;
		return input.substring(offset, offset + 1).toUpperCase().equals(input.substring(offset, offset + 1))
				&& !input.toLowerCase().equals(input)
				&& !input.toUpperCase().equals(input)
				&& !input.contains("_")
				&& !input.contains("-");
	}

	@Override
	public String transform(String[] input) {
		String transformed = super.transform(input);
		int offset = transformed.charAt(0) == '$' ? 1 : 0;

		return (offset == 0 ? "" : "$")
				+ transformed.substring(offset, offset + 1).toUpperCase()
				+ transformed.substring(offset + 1);
	}
}
