package nl.makertim.jetbrains.casetoggler.casing;

public class KebabCase extends CharSeparatedCase {

	public KebabCase() {
		super("-", "_");
	}
}
