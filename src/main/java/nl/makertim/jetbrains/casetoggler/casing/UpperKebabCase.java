package nl.makertim.jetbrains.casetoggler.casing;

public class UpperKebabCase extends CharSeparatedCase {

	public UpperKebabCase() {
		super("-", "_");
	}

	@Override
	public boolean matches(String input) {
		return input.toUpperCase().equals(input)
				&& input.contains(separator);
	}

	@Override
	public String transform(String[] input) {
		return super.transform(input).toUpperCase();
	}
}
