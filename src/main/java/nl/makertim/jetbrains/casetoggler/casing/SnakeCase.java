package nl.makertim.jetbrains.casetoggler.casing;

public class SnakeCase extends CharSeparatedCase {

	public SnakeCase() {
		super("_", "-");
	}
}
