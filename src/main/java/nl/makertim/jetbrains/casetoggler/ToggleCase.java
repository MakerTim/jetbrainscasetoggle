package nl.makertim.jetbrains.casetoggler;

import com.intellij.lang.Language;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Caret;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiNamedElement;
import com.intellij.psi.PsiReference;
import com.intellij.refactoring.rename.RenameUtil;
import nl.makertim.jetbrains.casetoggler.casing.Casing;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Objects;

public class ToggleCase extends AnAction {

	@Override
	public void actionPerformed(@NotNull AnActionEvent anActionEvent) {
		Project project = anActionEvent.getProject();
		Editor editor = anActionEvent.getData(CommonDataKeys.EDITOR);

		if (project == null || editor == null) {
			return;
		}

		PsiFile file = anActionEvent.getRequiredData(CommonDataKeys.PSI_FILE);

		WriteCommandAction.runWriteCommandAction(project, () -> {
			Caret[] caretsThatAreNotRenamed = tryRenamePsiElement(project, editor, file);
			for (Caret caret : caretsThatAreNotRenamed) {
				renameStringAtCaret(project, editor, caret);
			}
		});
	}

	private Caret[] tryRenamePsiElement(Project project, Editor editor, PsiFile file) {
		//noinspection SuspiciousToArrayCall
		return editor.getCaretModel().getAllCarets().stream()
				.map(caret -> getNamedElementFromCaretOrCaret(file, caret))
				.map(namedElementOrCaret -> {
					if (namedElementOrCaret instanceof Caret) {
						return namedElementOrCaret;
					}
					PsiNamedElement namedElement = (PsiNamedElement) namedElementOrCaret;
					String oldName = namedElement.getName();
					String newName = Casing.transformToNextCasing(oldName, namedElement.getLanguage());
					RenameUtil.doRename(namedElement, newName,
							RenameUtil.findUsages(
									namedElement,
									newName,
									namedElement.getUseScope(),
									false,
									false,
									new HashMap<>()),
							project, null);
					return null;
				})
				.filter(Objects::nonNull)
				.toArray(Caret[]::new);
	}

	private void renameStringAtCaret(Project project, Editor editor, Caret caret) {
		Document document = editor.getDocument();

		String selectedString = editor.getSelectionModel().getSelectedText();
		if (selectedString == null || selectedString.isEmpty()) {
			editor.getSelectionModel().selectWordAtCaret(false);
			selectedString = editor.getSelectionModel().getSelectedText();
			if (selectedString == null || selectedString.isEmpty()) {
				return;
			}
		}

		String newString = Casing.transformToNextCasing(selectedString, Language.ANY);
		int startSelection = caret.getSelectionStart();
		int endSelection = caret.getSelectionEnd();

		WriteCommandAction.runWriteCommandAction(project,
				() -> document.replaceString(startSelection, endSelection, newString)
		);
	}

	private Object getNamedElementFromCaretOrCaret(PsiFile file, Caret caret) {
		PsiElement caretElement = file.findElementAt(caret.getOffset() - 1);
		if (caretElement == null) {
			return caret;
		}
		if (!(caretElement instanceof PsiNamedElement)) {
			caretElement = caretElement.getParent();
		}
		if (!(caretElement instanceof PsiNamedElement)) {
			PsiReference reference = caretElement.getReference();
			if (reference != null || (reference = caretElement.getFirstChild().getReference()) != null) {
				PsiElement resolvedReference = reference.resolve();
				if (resolvedReference instanceof PsiNamedElement) {
					return resolvedReference;
				}
			}
			return caret;
		}
		return caretElement;
	}
}
